# coding: utf-8

# Se pueden sacar de: https://en.wikipedia.org/wiki/List_of_Wikipedias#List
langs = ["es", "en", "pt"]

# Config:
num_threads = 50

path_data = "."

# wikimedia sparql queries.
# Usamos el old formating '%' porque la query tiene llaves

# Hacemos una query para saber que entidad es cada pais
# Traemos todo lo que sea instancia (P31) de estado soberano (Q3624078)
# Podemos traer filtrar solamente los de una region con:
# que sean parte (P361) de sur america (Q18):
#   wdt:P361 wd:Q18.

# Si usamos pais (Q6256) no nos traemos algunos importantes,
# como españa y francia. Por eso usamos estado soberano (Q3624078)

query_country = """
SELECT ?country ?countryLabel
WHERE {
  ?country wdt:P31 wd:Q3624078;
  SERVICE wikibase:label { bd:serviceParam wikibase:language "%(lang)s". }
}
"""

# Traemos todo lo que sea instancia (P31) de Humano (Q5)
# tal que sexo o genero (P21) sea femenino (Q6581072)
# tal que tengan lugar de nacimiento (P19) y lo guardamos en la variable ?place
# tal que ?place se encuentre en el pais (P17) que le pasamos.
# y nos traemos las coordenadas del lugar (P625)
# y el url de la entidad en la lang de la wiki que le pedimos (schema:about)
query_woman_place_birth = """
SELECT ?people ?peopleLabel ?sitelink ?place ?placeLabel ?location
WHERE {
  ?people wdt:P31 wd:Q5;
          wdt:P21 wd:Q6581072;
          wdt:P19 ?place.
  ?place wdt:P17 wd:%(entitie)s;
         wdt:P625 ?location.

  ?sitelink schema:about ?people.
  ?sitelink schema:isPartOf <https://%(lang)s.wikipedia.org/>.

  SERVICE wikibase:label { bd:serviceParam wikibase:language "%(lang)s". }
}
"""

# igual que la anterior, pero sin el SERVICE al final.
# No trae las Labels, pero hace la query mucho mas rapida (en especial las grandes).
query_woman_place_birth_no_service = """
SELECT ?people ?sitelink ?place ?location

WHERE {
  ?people wdt:P31 wd:Q5;
          wdt:P21 wd:Q6581072;
          wdt:P19 ?place.
  ?place wdt:P17 wd:%(entitie)s;
         wdt:P625 ?location.

  ?sitelink schema:about ?people.
  ?sitelink schema:isPartOf <https://%(lang)s.wikipedia.org/>.
}
"""
