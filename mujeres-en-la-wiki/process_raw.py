import json
from pathlib import Path

import pandas as pd

import config


MIN_VISIT = 500


def find_entitie_flag(country, lang):
    """Encuentra la bandera dado el pais y el lang"""
    for k, v in data_flag.items():
        if lang in v and v[lang] == country:
            flag = v["flag"]
            return flag
    return ""


p_data = Path(config.path_data)
p_raw = p_data / "raw"
p_procesada = p_data / "procesada"

# Borramos la ultima salida.
for p in p_procesada.iterdir():
    p.unlink()

with open(p_raw / "info_banderas.json") as f:
    data_flag = json.load(f)

# Creamos el mapeo flag -> int
# flags_to_int = {}

# for i, v in enumerate(data_flag.values()):
#    flag = v["flag"]
#    if flag:
#        flags_to_int[flag] = i

# flags_to_int[""] = -1

# lo damos vuelta para guardar
# int_to_flag = {v: k for k, v in flags_to_int.items()}

# with open(p_procesada / "int_to_flag.json", "w") as f:
#     json.dump(int_to_flag, f)


uniques = list(
    i.name.split("_", maxsplit=1)[1][:-4]
    for i in p_raw.iterdir()
    if i.name.startswith("query")
)
langs = list(set(unique.split("_")[-1] for unique in uniques))

total_views_by_country = pd.DataFrame()


for lang in langs:
    out_name = p_procesada / f"mujeres_{lang}.csv"

    total_views_by_country = pd.DataFrame()

    for unique in [unique for unique in uniques if unique.endswith(lang)]:

        country = unique.split("_")[0]

        df_query = pd.read_csv(p_raw / f"query_{unique}.csv")
        df_pageviews = pd.read_csv(p_raw / f"pageviews_mujeres_{unique}.csv").drop(
            "people", axis=1
        )

        assert df_query.shape[0] == df_pageviews.shape[0]

        # Tiramos las primeras col hasta que tengamos multiplo de 3
        while len(df_pageviews.columns) % 3 != 0:
            df_pageviews = df_pageviews.drop(df_pageviews.columns[1], axis=1)

        # Sumamos cada 3 meces
        df_pageviews_reduce = pd.DataFrame()
        for i in range(df_pageviews.shape[1] // 3):
            col_name = df_pageviews.columns[(i * 3) + 1]
            col_value = df_pageviews.iloc[:, i * 3 : (i + 1) * 3].sum(axis=1)
            df_pageviews_reduce[col_name] = col_value

        df_pageviews = df_pageviews_reduce
        del df_pageviews_reduce

        # Si tiene bandera, calculamos la suma total del pais por unidad de tiempo.
        flag_country = find_entitie_flag(country, lang)
        if flag_country:
            total_views_by_country[flag_country] = df_pageviews.sum()

        # Le agregamos el int del pais
        df_query["country_flag"] = flag_country

        df = pd.concat([df_query, df_pageviews], axis=1)

        total_views = df_pageviews.sum(axis=1)
        df["total_views"] = total_views

        df = (
            df.sort_values(["placeLabel", "total_views"], ascending=False)
            .groupby("placeLabel")
            .head(1)
        )
        df = df.drop("total_views", axis=1)
        df = df.drop(["people", "peopleLabel", "place", "placeLabel"], axis=1)
        # Solo las que para algun chunk de meses superen el MIN_VISIT
        df = df[(df.iloc[:, 4:] > MIN_VISIT).any(axis=1)]
        df = df.dropna()
        
        if not (out_name).exists():
            df.to_csv(out_name, index=False)
        else:
            df.to_csv(out_name, index=False, header=False, mode="a")

    total_views_by_country.T.to_csv(
        p_procesada / f"total_views_by_country_{lang}.csv", index_label="country"
    )


# Sacamos como json el valor maximo de todos los datos
# lo usamos para la escala del plot
max_value = 0

for f in p_raw.iterdir():
    df = pd.read_csv(f)
    max_value_tmp = df.iloc[:, 3:].max(axis=1).max()

    if max_value_tmp > max_value:
        max_value = max_value_tmp

with open(p_procesada / "max_value.json", "w") as f:
    json.dump({"max": int(max_value)}, f)
