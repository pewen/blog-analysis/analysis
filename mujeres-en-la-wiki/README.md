# Mapa mujeres wikimedia

Scrips para bajar y procesar los datos necesarios para la [vis del blog](https://pewen.gitlab.io/es/blog/sismos-en-vaca-muerta-anelo-sauzal-bonito-y-la-region/) sobre visitas en paginas de mujeres en la wikipedia.  

![Screenshot vis](Screenshot_mapa_mujeres.png)

Baja las entidades de wikidata corriendo una query (ubicadas tambien en el `config.py`) por cada pais.
Procesa todo lo bajado y lo guarda en el directorio `procesada/` con :
- int_to_flag.json: para mapear de la columna `country_index` al emoji de bandera correspondiente.
- mujeres_[lang].csv:
    - sitelink: el final del url (resource) al articulo de la wikipedia.
    - lat: latitud de la ciudad.
    - log: longitud de la ciudad.
    - country_index: indice a la bandera, para usar con `int_to_flag.json`
    - fechas: cantidad de visitas para esa pagina cada tres meses en el idioma correspondiente.
- total_views_by_country_[lang].csv:
    - country: emoji de la bandera correspondiente.
    - fechas: cantidad total de visitas a paginas de mujeres para ese pais en el idioma.

## Instalación

``` bash
pip install -r requirements.txt
```

## Configuración

Se definen los idiomas a bajar en el archivo `config.py`:

```
# En config.py
langs = ['es', 'en', 'pt']
```

Donde los idiomas tienen que ser codigos de [wikipedia validos](https://en.wikipedia.org/wiki/List_of_Wikipedias#List).

Es posible crear un mapa diferente para otra entidad solo cambiando las queries. Para mas info mirar el tutorial de [SPARQL](https://www.wikidata.org/wiki/Special:MyLanguage/Wikidata:SPARQL_tutorial).


## Ejecutar

Por defecto no vuelve a bajar si ya existe en el directorio `raw/`, hay que borralo para forzar bajar data nueva.

``` bash
python download_raw.py && python process_raw.py
```
