# coding: utf-8

import time
import json
from pathlib import Path
from urllib.parse import unquote
from datetime import datetime, timedelta
from multiprocessing.pool import ThreadPool

import requests
import pandas as pd
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

import config


DEFAULT_TIMEOUT = 10
LONG_TIMEOUT = 65  # sparql tiene un timeout de 60

url = "https://query.wikidata.org/sparql"
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"
}
params = {"format": "json", "query": ""}

langs = config.langs
num_threads = config.num_threads

retry_strategy = Retry(backoff_factor=1)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)

def get_last_path(url: str) -> str:
    """Return the last path from a url.
    Example:

    >>> get_last_path("https://es.wikipedia.org/wiki/Kaori_Utatsuki")
    Kaori_Utatsuki"""
    return url.rsplit("/", maxsplit=1)[-1]


def people_to_df(people):
    """Create df from the people dict and pre-process data."""
    df = pd.DataFrame(people)
    df.sitelink = df.sitelink.apply(unquote)
    df.people = df.people.apply(get_last_path)
    df.place = df.place.apply(get_last_path)
    df.sitelink = df.sitelink.apply(get_last_path)

    # Truncamos a una cantidad de digitos que tenga sentido: https://www.xkcd.com/2170/
    df.location = df.location.apply(lambda x: x[6:-1])
    location = df.location.str.split(expand=True)
    location.columns = ["lng", "lat"]

    df = df.drop("location", axis=1)
    df["lat"] = location.lat
    df["lng"] = location.lng
    df.lat = df.lat.astype("float").apply(lambda x: round(x, 4))
    df.lng = df.lng.astype("float").apply(lambda x: round(x, 4))

    return df


def fabric_get_entities(lang: str):
    """Utilidad para crear la funcion que va a usar el Pool de threads en paralelo."""

    def get_entities(entities: list) -> dict:
        """Return the labels of the entities"""
        url_api = "https://www.wikidata.org/w/api.php"

        params = {
            "action": "wbgetentities",
            "format": "json",
            "props": "labels",
            "ids": "|".join(entities),
            "languages": lang,
        }

        r = http.get(url_api, params=params, timeout=DEFAULT_TIMEOUT)
        data = r.json()

        return {
            k: v["labels"][lang]["value"]
            for k, v in data["entities"].items()
            if v["labels"]
        }

    return get_entities


def fabric_get_valor(lang: str):
    """Utilidad para crear la funcion que va a usar el Pool de threads en paralelo."""
    url = (
        "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/"
        "lang.wikipedia/all-access/user/{name}/monthly/2015070100/2020022900"
    ).replace("lang", lang)

    def get_valor(name):
        """Devuelve la cantidad de visitas por mes para la pagina de wiki correspondiente."""
        r = http.get(url.format(name=name), timeout=DEFAULT_TIMEOUT)
        if r.status_code != 200:
            return name
        return [d["views"] for d in r.json()["items"]]

    return get_valor


def is_flag(flag: str) -> bool:
    """
    Devuelve True si es un emoji de una bandera.
    Mas info: https://en.wikipedia.org/wiki/Regional_Indicator_Symbol
    """
    if len(flag) != 2:
        return False

    return all(True if i <= "🇿" and i >= "🇦" else False for i in flag)


def get_flag(entities: str) -> str:
    """Intenta conseguir el emoji de la bandera del pais. Devuelve '' si no lo encuentra."""
    url_api = "https://www.wikidata.org/w/api.php"

    params = {
        "action": "wbgetentities",
        "format": "json",
        "props": "aliases",
        "ids": entities,
    }

    r = http.get(url_api, params=params, timeout=DEFAULT_TIMEOUT)
    data = r.json()

    for entitie in data["entities"].values():
        for alias in entitie["aliases"].values():
            for value in alias:
                posible_flag = value["value"]
                if is_flag(posible_flag):
                    return posible_flag

    return ""


# ISO-8601 y-m-d
start_date = "2015-07-01"
end_date = datetime.today()
actual_month = end_date.month

while actual_month == end_date:
    end_date -= timedelta(days=1)

end_date = "-".join(str(i) for i in [end_date.year, end_date.month, end_date.day])

dates = pd.date_range(start_date, end_date, freq="M")
len_dates = dates.shape[0]

p_data = Path(config.path_data)
p_raw = p_data / "raw"
p_procesada = p_data / "procesada"

p_raw.mkdir(parents=True, exist_ok=True)
p_procesada.mkdir(parents=True, exist_ok=True)

data_flags = {}

for lang in langs:
    # Traemos los paices que esten en ese idioma.
    params["query"] = config.query_country % {"lang": lang}
    r = http.get(url, params=params, headers=headers, timeout=DEFAULT_TIMEOUT)
    data = r.json()

    countries = []
    for item in data["results"]["bindings"]:
        country = item["country"]["value"].rsplit("/", maxsplit=1)[-1]
        country_label = item["countryLabel"]["value"]
        #  Removemos los que no tengan un nombre en ese idioma (no son importantes)
        if country != country_label:
            countries.append((country, country_label))

    print(f"Idiomas a bajar: {lang}")
    print("Paises a bajar: ")
    for _, country in countries:
        print(country, end=" ")
    print(f"\n\nEn total {len(countries)} paises.")

    print("\n Banando las banderas de los paices...", end="")
    # Traemos los emojis de la bandera de cada pais.
    list_entities = [i[0] for i in countries]

    with ThreadPool(num_threads) as pool:
        flags = pool.map(get_flag, list_entities)

    # Actualizamos el dict con la info.
    for i, flag in zip(countries, flags):
        Q = i[0]
        name = i[1]
        if Q not in data_flags:
            data_flags.update({Q: {lang: name, "flag": flag}})
        else:
            info = data_flags[Q]
            info.update({lang: name})
            data_flags[Q] = info

    print("\tDone")

    # Pedimos para cada pais
    timeout_countries = []

    for entitie, country in countries:
        out_file = p_raw / f"query_{country}_{lang}.csv"

        if out_file.exists():
            print(f"{country} ya esta bajado, skip.")
            continue

        print(f"Bajando {country}", end="")
        t1 = time.time()
        params["query"] = config.query_woman_place_birth % (
            {"entitie": entitie, "lang": lang}
        )
        r = http.get(url, params=params, headers=headers, timeout=LONG_TIMEOUT)
        t2 = time.time()
        print(f"\t{t2 - t1:.2f} sec", end="")
        try:
            data = r.json()
        except:
            print("\t error")
            timeout_countries.append((entitie, country))
            continue

        people = []
        for item in data["results"]["bindings"]:
            people.append({key: item[key]["value"] for key in data["head"]["vars"]})

        if people:
            df = people_to_df(people)
            df.to_csv(out_file, index=False)
            print("\t done!")
        else:
            print("\t no people in country")

    print(
        f'\n Fallo en {len(timeout_countries)} paises.\n Reintentando sin el "SERVICE".'
    )

    for entitie, country in timeout_countries:
        out_file = p_raw / f"query_{country}_{lang}.csv"

        if out_file.exists():
            print(f"{country} ya esta bajado, skip.")
            continue

        data = {}
        print(f"Bajando {country}", end="")

        while not data:
            t1 = time.time()
            params["query"] = config.query_woman_place_birth_no_service % {
                "entitie": entitie,
                "lang": lang,
            }
            r = http.get(url, params=params, headers=headers, timeout=LONG_TIMEOUT)
            t2 = time.time()
            print(f"\t{t2 - t1:.2f} sec", end="")
            try:
                data = r.json()
            except:
                print("\ttimeout error. wait 30 seconds and retry.", end="")
                time.sleep(30)

        people = []
        for item in data["results"]["bindings"]:
            people.append({key: item[key]["value"] for key in data["head"]["vars"]})

        if people:
            df = people_to_df(people)

        # Hidratamos las entidades para conseguir los Labels
        print("\t Hidratando", end="")
        for col in ["people", "place"]:
            uniques = df[col].unique()
            len_uniques = uniques.shape[0]

            list_entities = [
                uniques[50 * i : (i + 1) * 50] for i in range((len_uniques // 50) + 1)
            ]

            # creamos la funcion para el idioma que queremos
            get_entities = fabric_get_entities(lang)

            t1 = time.time()
            with ThreadPool(num_threads) as pool:
                chunks_labels = pool.map(get_entities, list_entities)

            t2 = time.time()
            print(f"\t{t2 - t1:.2f} sec", end="")

            labels = {}
            for chunk in chunks_labels:
                labels.update(chunk)

            df[col + "Label"] = df[col].apply(lambda x: labels.get(x, x))

        # rearange columns
        df = df[
            ["people", "peopleLabel", "sitelink", "place", "placeLabel", "lat", "lng"]
        ]
        df.to_csv(out_file, index=False)
        print("\t done!")

    print("\n\nBajando los pageviews de cada pagina.")
    for raw_file in (
        i
        for i in p_raw.iterdir()
        if i.name.startswith("query_") and i.name.endswith(f"_{lang}.csv")
    ):
        country = raw_file.name.split("_")[1]

        out_file = p_raw / f"pageviews_mujeres_{country}_{lang}.csv"

        if out_file.exists():
            print(f"{country} ya esta bajado, skip.")
            continue

        print(f"Bajando para: {country}", end="")
        df = pd.read_csv(raw_file)

        # La lista con los nombres de los articulos que vamos a buscar
        names = df.sitelink

        # creamos la funcion para el idioma que queremos
        get_valor = fabric_get_valor(lang)

        print("\tTrafico de las paginas ...", end="")
        # No debe superar los 200 requests por segundo
        t0 = time.time()
        with ThreadPool(num_threads) as pool:
            valores = pool.map(get_valor, names)

        t1 = time.time()
        print(
            "\t{} bajados, {:.2f} requests por segundo ".format(
                len(valores), len(valores) / (t1 - t0)
            ),
            end="",
        )

        # Si tenemos algun error, lo separamos para despues
        errores = []
        valores_ = []
        for valor in valores:
            if type(valor) != list:
                errores.append(valor)
                valor = [0] * len_dates
            # Si tiene menos elementos que "len_dates" hacemos padding con ceros
            if len(valor) != len_dates:
                valor = [0] * (len_dates - len(valor)) + valor
            valores_.append(valor)

        valores = valores_

        if errores:
            print("\t{} Errores en el trafico".format(len(errores)), end="")
            # Como no hacemos nada con esto por ahora, lo obviamos.
            # pd.DataFrame(errores).to_csv(f'datos/errores_{country}_{lang}.csv', index=False, header=False)

        df_values = pd.DataFrame(valores, index=df.people, columns=dates)
        df_values.to_csv(out_file)
        print("\tDone")

# Guardamos la info de las banderas.
# Algunos que faltan en wikidata.
data_flags["Q159"]["flag"] = "🇷" + "🇺"
data_flags["Q34"]["flag"] = "🇸" + "🇪"

print("\nSalvando la info de las banderas", end="")

with open(p_raw / "info_banderas.json", "w") as f:
    json.dump(data_flags, f)

print("\tDone!")
