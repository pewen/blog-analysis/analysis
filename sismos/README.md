# Sismos

Preprocesado para la nota de
[sismos](https://pewen.gitlab.io/es/blog/sismos-en-vaca-muerta-anelo-sauzal-bonito-y-la-region/),
usando los [datos](https://gitlab.com/pewen/scrapers/inpres/-/tree/master/data/inpres) del [scraper](https://gitlab.com/pewen/scrapers/inpres) del [inpres](https://www.inpres.gob.ar/desktop/).
